package com.example.blood;

import android.media.AudioFormat;

public class Constant {
	public static final int SampleRate = 44100;
	public static final int Format = AudioFormat.ENCODING_PCM_16BIT;

}
