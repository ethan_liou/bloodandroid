package com.example.blood;

public class Data {
	float[] data;
	int length;
	boolean leftChannel;
	public Data(int length) {
		this.length = length;
		data = new float[length];
	}	
	
	public Data(float[] buffer, int length,boolean leftChannel) {
		this.length = length;
		data = buffer.clone();
		this.leftChannel = leftChannel;
	}	
}
