package com.example.blood;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import android.app.Activity;
import android.util.Log;

public class DrawThread extends Thread {
	DrawView mView;
	LinkedBlockingQueue<Data> mDataQueue; // for painting...
	FloatBuffer mLeftBuffer;
	FloatBuffer mRightBuffer;
	boolean mStop;
	Activity mActivity;
	float[] mLeftData;
	float[] mRightData;
	
	public DrawThread(DrawView view, LinkedBlockingQueue<Data> mDataQueue,Activity activity) {
		super();
		this.mView = view;
		this.mDataQueue = mDataQueue;
		mActivity = activity;
		mLeftBuffer = FloatBuffer.allocate(8000*10);
		mRightBuffer = FloatBuffer.allocate(8000*10);
		mLeftData = new float[4000];
		mRightData = new float[4000];
	}
	
	@Override
	public void interrupt() {
		mStop = true;
		super.interrupt();
	}



	@Override
	public void run() {
		super.run();
		mStop = false;
		File f = new File(mActivity.getExternalCacheDir(),""+System.currentTimeMillis()+".data");
		FileWriter fw = null;
		try {
			fw = new FileWriter(f);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while(!mStop){
			try {
				Data data = mDataQueue.take();
				Log.d("Data",""+mLeftBuffer.position()+","+mRightBuffer.position());
				FloatBuffer buffer = data.leftChannel ? mLeftBuffer : mRightBuffer;
				buffer.put(data.data, 0, data.length);
				
				if(mLeftBuffer.position() > 4000 && mRightBuffer.position() > 4000){
					Log.i("Full","Trigger");
					
					// left
					mLeftBuffer.limit(mLeftBuffer.position());
					mLeftBuffer.rewind();
					mLeftBuffer.get(mLeftData);
					mLeftBuffer.compact();
					float max = Float.MIN_VALUE;
					boolean raw = true;
					if(raw){
						for(int i = 0 ; i < 400 ; i ++){
							mView.mLeftData[i] = mLeftData[i];
						}
					} else{
						for(int i = 0 ; i < 400 ; i ++){
							mView.mLeftData[i] = mLeftData[i*10];
						}						
					}

					// right
					mRightBuffer.limit(mRightBuffer.position());
					mRightBuffer.rewind();
					mRightBuffer.get(mRightData);
					mRightBuffer.compact();
					max = Float.MIN_VALUE;
					if(raw){
						for(int i = 0 ; i < 400 ; i ++){
							mView.mRightData[i] = mRightData[i];
						}
					} else{
						for(int i = 0 ; i < 400 ; i ++){
							mView.mRightData[i] = mRightData[i*10];
						}						
					}
					
					mActivity.runOnUiThread(new Runnable() {						
						@Override
						public void run() {
							mView.invalidate();
						}
					});
				}
				
				for(int i = 0; i < 400 ; i++){
					fw.write(""+mView.mLeftData[i]+","+mView.mRightData[i]+"\n");
				}
			} catch (InterruptedException e) {
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 	
		}
		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
