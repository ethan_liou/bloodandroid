package com.example.blood;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class DrawView extends View {
	public float[] mLeftData;
	public float[] mRightData;
	Paint mLeftPaint;
	Paint mRightPaint;
	Paint mAxisPaint;
	
	public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		mLeftPaint = new Paint();
		mLeftPaint.setAntiAlias(true);
		mLeftPaint.setColor(Color.RED);
		mLeftPaint.setStrokeWidth(1.5f);
		mRightPaint = new Paint(mLeftPaint);
		mRightPaint.setColor(Color.BLUE);
		mAxisPaint = new Paint(mLeftPaint);
		mAxisPaint.setColor(Color.BLACK);
	}

	public DrawView(Context context, AttributeSet attrs) {
		this(context,attrs,0);
	}

	public DrawView(Context context) {
		this(context,null);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if(mLeftData != null && mRightData != null){
			int centerH = canvas.getHeight() / 2;
			canvas.drawLine(0, centerH , canvas.getWidth(), centerH , mAxisPaint);
			int factor = 1;
			for(int i = 0 ; i < mLeftData.length - 1 ; i++){
				canvas.drawLine(i, centerH*(1 - factor*mLeftData[i]), (i+1), centerH * (1 - factor*mLeftData[i+1]), mLeftPaint);
				canvas.drawLine(i, centerH*(1 - factor*mRightData[i]), (i+1), centerH * (1 - factor*mRightData[i+1]), mRightPaint);
			}
		}
	}
}
