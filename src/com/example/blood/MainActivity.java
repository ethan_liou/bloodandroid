package com.example.blood;

import java.util.concurrent.LinkedBlockingQueue;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.filters.LowPassFS;
import be.tarsos.dsp.filters.LowPassSP;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;
import be.tarsos.dsp.pitch.PitchProcessor.PitchEstimationAlgorithm;
import android.support.v7.app.ActionBarActivity;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements OnClickListener {
	LinkedBlockingQueue<Data> mDataQueue; // for painting...
	PlayThread mPlayThread;
	DrawThread mDrawThread;
	DrawView mDrawView;
	Thread mRecordThread;
	AudioDispatcher mDispatcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);

		((Button) findViewById(R.id.start_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.switch_btn))
				.setOnClickListener(this);
		((Button) findViewById(R.id.stop_btn)).setOnClickListener(this);
		findViewById(R.id.start_btn).setEnabled(true);
		findViewById(R.id.stop_btn).setEnabled(false);
		
		mDataQueue = new LinkedBlockingQueue<Data>();
		mDrawView = (DrawView) findViewById(R.id.drawView);
		mDrawView.mLeftData = new float[441];
		mDrawView.mRightData = new float[441];
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	
	@Override
	protected void onDestroy() {
		onClick(findViewById(R.id.stop_btn));
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.start_btn) {
			findViewById(R.id.start_btn).setEnabled(false);
			findViewById(R.id.stop_btn).setEnabled(true);
			mPlayThread = new PlayThread(100,100);
			mDrawThread = new DrawThread(mDrawView, mDataQueue, this);
			mDrawThread.start();
			mPlayThread.start();
			
			// record
			int RecordSampleRate = 8000;
			int minAudioBufferSize = AudioRecord.getMinBufferSize(RecordSampleRate,
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT);

			mDispatcher = AudioDispatcherFactory.fromDefaultMicrophone(RecordSampleRate,minAudioBufferSize,0);
			mDispatcher.addAudioProcessor(new SplitProcessor());
//			LowPassSP lpf = new LowPassSP(3, RecordSampleRate);
//			LowPassFS lpf = new LowPassFS(3, RecordSampleRate);
//			mDispatcher.addAudioProcessor(lpf);
			
			mDispatcher.addAudioProcessor(new AudioProcessor() {
				int counter = 0;
				
				@Override
				public void processingFinished() {}
				
				@Override
				public boolean process(AudioEvent audioEvent) {
					float[] buffer = audioEvent.getFloatBuffer();
					if(counter != 0){
						try {
							mDataQueue.put(new Data(buffer,buffer.length,counter++ % 2 == 0));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}						
					} else {
						counter ++;
					}
					return true;
				}
			});
			mRecordThread = new Thread(mDispatcher,"Audio Dispatcher");
			mRecordThread.start();
			
			findViewById(R.id.start_btn).setEnabled(false);
			findViewById(R.id.stop_btn).setEnabled(true);
		} else if (v.getId() == R.id.stop_btn) {
			try {
				if(mPlayThread != null){
					mPlayThread.interrupt();
					mPlayThread.join();					
				}
				mDispatcher.stop();
			} catch (Exception e) {
			} finally {
				findViewById(R.id.start_btn).setEnabled(true);
				findViewById(R.id.stop_btn).setEnabled(false);
			}
		} else if (v.getId() == R.id.switch_btn){
			if(mPlayThread != null){
				mPlayThread.mOneSignal = mPlayThread.mOneSignal ? false : true;
				Toast.makeText(this, "One signal "+mPlayThread.mOneSignal, Toast.LENGTH_SHORT).show();
				mPlayThread.GenerateBuffer();				
			}
		}
	}
	
	class SplitProcessor implements AudioProcessor{
		float[] firstData;
		float[] secondData;
		
		public SplitProcessor() {
			super();
			firstData = null;
			secondData = null;
		}

		@Override
		public boolean process(AudioEvent audioEvent) {
			float[] data = audioEvent.getFloatBuffer();
			if(firstData == null && secondData == null){
				// first...
				secondData = new float[data.length];
				firstData = audioEvent.getFloatBuffer().clone();
			} else if(firstData != null){
				int leftIdx = 0;
				int rightIdx = 0;
				float[] backup = new float[data.length]; // TODO enhance?
				// left write to backup => data
				// right write to second data
				
				// already have left & right channel, return left and save right in second...
				for(int i = 0 ; i < firstData.length ; i++){
					if(i % 2 == 0){
						backup[leftIdx++] = firstData[i];
					} else {
						secondData[rightIdx++] = firstData[i];
					}
				}
				for(int i = 0 ; i < data.length ; i++){
					if(i % 2 == 0){
						backup[leftIdx++] = data[i];
					} else {
						secondData[rightIdx++] = data[i];
					}
				}
				System.arraycopy(backup, 0, data, 0, data.length);
				firstData = null;
			} else {
				// return right channel
				firstData = audioEvent.getFloatBuffer().clone();
				System.arraycopy(secondData, 0, data, 0, data.length);
			}
			return true;
		}

		@Override
		public void processingFinished() {}
	}
	
	
}
