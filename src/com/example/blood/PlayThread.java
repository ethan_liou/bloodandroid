package com.example.blood;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

public class PlayThread extends Thread {
	boolean mIsPlayering;
	AudioTrack mPlayer;
	short[] mBuffer;
	public boolean mOneSignal;

	public void GenerateBuffer() {
		for (int i = 0; i < 441; i++) {
			for (int j = 0; j < 200; j++) {
				if(mOneSignal && (j % 4 == 0 || j % 4 == 3)){
					mBuffer[i * 200 + j] = 0;
					continue;
				}
				double arg = Math.PI * 2 / 100 * (j/2);
				double sin = Math.sin(arg);
				double val = 32767 * sin;
				mBuffer[i * 200 + j] = (short) val;
			}
		}			
	}

	public PlayThread(int lHz, int rHz) {
		super();
		// int playerBufSize = AudioTrack.getMinBufferSize(Constant.SampleRate,
		// AudioFormat.CHANNEL_OUT_STEREO, Constant.Format);
		mOneSignal = true;
		
		// 44100
		mBuffer = new short[88200];
		GenerateBuffer();
		for (int i = 0; i < 400; i++) {
			Log.i("Buffer", "" + i + "," + mBuffer[i]);
		}

		mPlayer = new AudioTrack(AudioManager.STREAM_MUSIC,
				Constant.SampleRate, AudioFormat.CHANNEL_OUT_STEREO,
				Constant.Format, mBuffer.length, AudioTrack.MODE_STREAM);
		mPlayer.setPlaybackRate(Constant.SampleRate);
	}

	@Override
	public void interrupt() {
		mIsPlayering = false;
		super.interrupt();
	}

	@Override
	public void run() {
		mPlayer.play();
		mIsPlayering = true;
		while (mIsPlayering) {
			try {
				mPlayer.write(mBuffer, 0, mBuffer.length);					
			} catch (Exception e) {
			}
		}
		mPlayer.stop();
		mPlayer.release();
	}
}
